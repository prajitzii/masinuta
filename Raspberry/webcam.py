import cv2
from PIL import Image, ImageTk


class WebCam:
    def __init__(self, width, height):
        self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    def get_image(self):
        _, frame = self.cap.read()
        frame = cv2.flip(frame, 1)
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(image)
        imagetk = ImageTk.PhotoImage(image=img)
        return imagetk
