import tkinter as tk
import cv2
from PIL import Image, ImageTk
import numpy as np
import imutils

root = tk.Tk()
root.resizable(False, False)


class WebCam:
    def __init__(self, width, height):
        self.cap = self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    def get_image(self):
        _, frame = self.cap.read()
        frame = cv2.flip(frame, 1)
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(ImageProcessing.detect_rectangles(cv2image))
        imgtk = ImageTk.PhotoImage(image=img)
        return imgtk


class ImageProcessing:
    @classmethod
    def is_rectangle(cls, c):
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        if len(approx) == 4:

            return True
        else:
            return False

    @classmethod
    def detect_rectangles(cls, image):
        resized = imutils.resize(image, width=300)
        ratio = image.shape[0] / float(resized.shape[0])
        gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        thresh = cv2.threshold(blurred, 120, 255, cv2.THRESH_BINARY)[1]
        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        for c in cnts:
            is_rect = cls.is_rectangle(c)
            if is_rect:
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")
                cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        return image


class MainWindow(tk.Frame):
    def __init__(self, width, height, parent=None):
        tk.Frame.__init__(self, parent)
        self.wc = WebCam(width, height)
        self.label = tk.Label(root)
        self.label.pack()
        self.parent = parent
        self.pack()
        self.add_widgets()

    def add_widgets(self):
        self.winfo_toplevel().title("Webcam")

    def show_frame(self):
        imgtk = self.wc.get_image()
        self.label.imgtk = imgtk
        self.label.configure(image=imgtk)
        self.label.after(10, self.show_frame)


def camera():
    frame = MainWindow(512, 512)
    frame.show_frame()
    root.mainloop()


camera()
