import cv2
import imutils


class RectangleDetector:
    def __init__(self):
        pass

    def is_rectangle(self, c):
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        if len(approx) == 4:
            return True
        else:
            return False

    def detect_rectangles(self, image):
        resized = imutils.resize(image, width=300)
        ratio = image.shape[0] / float(resized.shape[0])

        gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]

        for c in cnts:
            m = cv2.moments(c)
            cx = int((m['m10'] / m['m00']) * ratio)
            cy = int((m['m01'] / m['m00']) * ratio)
            shape = self.is_rectangle(c)
            if shape:
                c = c.astype("float")
                c *= ratio
                c = c.astype("int")
                cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        return image


image = cv2.imread('shapes.png')
rd = RectangleDetector()
image = rd.detect_rectangles(image)
cv2.imshow("Image", image)
cv2.waitKey(0)