import tkinter as tk
import cv2
from PIL import Image, ImageTk
import os
import base64

root = tk.Tk()


class CameraFrame(tk.Frame):
    def __init__(self, file_name, image_max_width, image_max_height, parent=None):
        tk.Frame.__init__(self, parent)
        self.file_name = file_name
        self.image_max_width = image_max_width
        self.image_max_height = image_max_height
        width, height = 800, 600
        self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        self.lmain = tk.Label(root)
        self.lmain.pack()
        self.parent = parent
        self.pack()
        self.add_widgets()

    def add_widgets(self):
        self.winfo_toplevel().title("Webcam")
        cancel_button = tk.Button(self, text="Cancel", command=root.destroy)
        cancel_button.pack(padx=10, pady=10)
        width, height = cancel_button.size()
        take_picture_button = tk.Button(self, text="Take Picture", command=self.take_picture)
        take_picture_button.pack(padx=1 + width, pady=10)

    def show_frame(self):
        _, frame = self.cap.read()
        frame = cv2.flip(frame, 1)
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image=img)
        self.lmain.imgtk = imgtk
        self.lmain.configure(image=imgtk)
        self.lmain.after(10, self.show_frame)

    def take_picture(self):
        _, picture = self.cap.read()
        cv2.imwrite(self.file_name, picture)
        width, height = Image.open(self.file_name).size
        image = cv2.imread(self.file_name)
        fx = self.image_max_width / width
        fy = self.image_max_height / height
        image = cv2.resize(image, (0, 0), fx=fx, fy=fy)
        cv2.imwrite(self.file_name, image)
        root.destroy()


def camera(file_name, image_max_width, image_max_height):
    frame = CameraFrame(file_name, image_max_width, image_max_height, root)
    frame.show_frame()
    root.mainloop()


def take_picture(csrf_token):
    camera(csrf_token + '.png', 200, 200)
    width, height = Image.open(csrf_token + '.png').size
    print("Size: %d x %d" % (width, height))
    image_file = open(csrf_token + '.png', 'rb')
    content = base64.b64encode(image_file.read()).__str__()
    length = content.__len__()
    image_file.close()
    os.remove(csrf_token + '.png')
    return content[2:length - 1]