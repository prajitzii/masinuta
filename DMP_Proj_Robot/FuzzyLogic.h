///#include "HC_SR04.h"
#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>
 
// Step 1 -  Instantiating an object library
Fuzzy* fuzzy = new Fuzzy();

void setupRules(){
 Serial.begin(9600);
 
 // Step 2 - Creating a FuzzyInput distance
 FuzzyInput* distance = new FuzzyInput(1);// With its ID in param
 
 // Creating the FuzzySet to compond FuzzyInput distance
 FuzzySet* danger = new FuzzySet(0,0,70,75); //stopping distance
 distance->addFuzzySet(danger);
 FuzzySet* small = new FuzzySet(58, 65, 65, 70); // Small distance
 distance->addFuzzySet(small); // Add FuzzySet small to distance
 FuzzySet* safe = new FuzzySet(60, 75,85, 90); // Safe distance
 distance->addFuzzySet(safe); // Add FuzzySet safe to distance
 FuzzySet* big = new FuzzySet(75, 120, 200, 200); // Big distance
 distance->addFuzzySet(big); // Add FuzzySet big to distance
 
 fuzzy->addFuzzyInput(distance); // Add FuzzyInput to Fuzzy object
 
 // Passo 3 - Creating FuzzyOutput velocity
 FuzzyOutput* velocity = new FuzzyOutput(1);// With its ID in param
 
 // Creating FuzzySet to compond FuzzyOutput velocity
 FuzzySet* stopping = new FuzzySet(0,0,0,0);
 velocity->addFuzzySet(stopping); 
 FuzzySet* slow = new FuzzySet(40, 60, 70, 100); // Slow velocity
 velocity->addFuzzySet(slow); // Add FuzzySet slow to velocity
 FuzzySet* average = new FuzzySet(70, 110, 130, 150); // Average velocity
 velocity->addFuzzySet(average); // Add FuzzySet average to velocity
 FuzzySet* fast = new FuzzySet(130, 150, 255, 255); // Fast velocity
 velocity->addFuzzySet(fast); // Add FuzzySet fast to velocity
 
 fuzzy->addFuzzyOutput(velocity); // Add FuzzyOutput to Fuzzy object
 
 //Passo 4 - Assembly the Fuzzy rules
   // FuzzyRule "IF distance = danger THEN velocity = stopping"
 FuzzyRuleAntecedent* ifDistanceDanger = new FuzzyRuleAntecedent();
 ifDistanceDanger->joinSingle(danger);
 FuzzyRuleConsequent* thenVelocityStopping = new FuzzyRuleConsequent();
 thenVelocityStopping->addOutput(stopping);
 FuzzyRule* fuzzyRule00 = new FuzzyRule(11,ifDistanceDanger,thenVelocityStopping);
 fuzzy->addFuzzyRule(fuzzyRule00);

 
  // FuzzyRule "IF distance = samll THEN velocity = slow"
FuzzyRuleAntecedent* ifDistanceSmall = new FuzzyRuleAntecedent(); // Instantiating an Antecedent to expression
 ifDistanceSmall->joinSingle(small); // Adding corresponding FuzzySet to Antecedent object
 FuzzyRuleConsequent* thenVelocitySlow = new FuzzyRuleConsequent(); // Instantiating a Consequent to expression
 thenVelocitySlow->addOutput(slow);// Adding corresponding FuzzySet to Consequent object
 // Instantiating a FuzzyRule object
 FuzzyRule* fuzzyRule01 = new FuzzyRule(1, ifDistanceSmall, thenVelocitySlow); // Passing the Antecedent and the Consequent of expression
  
 fuzzy->addFuzzyRule(fuzzyRule01); // Adding FuzzyRule to Fuzzy object
  
 // FuzzyRule "IF distance = safe THEN velocity = normal"
 FuzzyRuleAntecedent* ifDistanceSafe = new FuzzyRuleAntecedent(); // Instantiating an Antecedent to expression
 ifDistanceSafe->joinSingle(safe); // Adding corresponding FuzzySet to Antecedent object
 FuzzyRuleConsequent* thenVelocityAverage = new FuzzyRuleConsequent(); // Instantiating a Consequent to expression
 thenVelocityAverage->addOutput(average); // Adding corresponding FuzzySet to Consequent object
 // Instantiating a FuzzyRule object
 FuzzyRule* fuzzyRule02 = new FuzzyRule(2, ifDistanceSafe, thenVelocityAverage); // Passing the Antecedent and the Consequent of expression
  
 fuzzy->addFuzzyRule(fuzzyRule02); // Adding FuzzyRule to Fuzzy object
  
 // FuzzyRule "IF distance = big THEN velocity = fast"
 FuzzyRuleAntecedent* ifDistanceBig = new FuzzyRuleAntecedent(); // Instantiating an Antecedent to expression
 ifDistanceBig->joinSingle(big); // Adding corresponding FuzzySet to Antecedent object
 FuzzyRuleConsequent* thenVelocityFast = new FuzzyRuleConsequent(); // Instantiating a Consequent to expression
 thenVelocityFast->addOutput(fast);// Adding corresponding FuzzySet to Consequent object
 // Instantiating a FuzzyRule object
 FuzzyRule* fuzzyRule03 = new FuzzyRule(3, ifDistanceBig, thenVelocityFast); // Passing the Antecedent and the Consequent of expression
  
 fuzzy->addFuzzyRule(fuzzyRule03); // Adding FuzzyRule to Fuzzy object
}

float computeVelocity(float distance)
{
    
  // Step 5 - Report inputs value, passing its ID and value
  fuzzy->setInput(1, distance); 
  // Step 6 - Exe the fuzzification
  fuzzy->fuzzify(); 
  // Step 7 - Exe the desfuzzyficação for each output, passing its ID
  float output = fuzzy->defuzzify(1);
  return output;
}


