#include <NewPing.h>

#define TRIGGER_PIN_MAIN  3
#define ECHO_PIN_MAIN     6
#define MAX_DISTANCE_MAIN 200
#define TRIGGER_PIN_LEFT 4
#define ECHO_PIN_LEFT 7
#define MAX_DISTANCE_LEFT 200 
#define TRIGGER_PIN_RIGHT 2
#define ECHO_PIN_RIGHT 5
#define MAX_DISTANCE_RIGHT 200
#define NUMBER_OF_MEASUREMENTS 10


NewPing mainSonar(TRIGGER_PIN_MAIN, ECHO_PIN_MAIN, MAX_DISTANCE_MAIN);
NewPing leftSonar(TRIGGER_PIN_LEFT, ECHO_PIN_LEFT, MAX_DISTANCE_LEFT);
NewPing rightSonar(TRIGGER_PIN_RIGHT, ECHO_PIN_RIGHT, MAX_DISTANCE_RIGHT);

float getDistance(NewPing sonar){
  int startTime = millis();
  int distance = sonar.ping_cm();
  int endTime = millis();
  int duration = endTime - startTime;
  if(duration >= 3 && distance == 0) {
    return 200;
  } else {
    return distance;
  }
}

float distance(NewPing sonar) {
  float sumOfMeasuredDistances = 0.0;
  for(int i = 0; i < NUMBER_OF_MEASUREMENTS; i++) {
    sumOfMeasuredDistances += getDistance(sonar);
  }
  return sumOfMeasuredDistances / NUMBER_OF_MEASUREMENTS;
}

float getMainSonarDistance() {
  return distance(mainSonar);
}

float getLeftSonarDistance() {
  return distance(leftSonar);
}

float getRightSonarDistance() {
  return distance(rightSonar);
}

