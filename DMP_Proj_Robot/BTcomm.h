#include <SoftwareSerial.h>
const byte rxPin = 10;
const byte txPin = 11;
// creerea unui obiect de tip software serial
SoftwareSerial bluetooth(rxPin, txPin);

void setupBT() {
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  Serial1.begin(115200);   // setare baud rate pentru conexiunea bluetooth
}

String readFromBT() {
  String readStr;
  while (Serial1.available()) {
    delay(3);  //delay to allow buffer to fill 
    if (Serial1.available() >0) {
      char c = Serial1.read();  //gets one byte from serial buffer
      readStr += c; //makes the string readString
    } 
  }
  if (readStr != ""){
    Serial.println(readStr);
  }
  return readStr;
}
void writeToBT(String stringToWrite){
  Serial1.print(stringToWrite);
}
