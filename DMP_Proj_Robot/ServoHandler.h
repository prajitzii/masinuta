#include <Servo.h>
#include "HC_SR04.h"

#define SERVO_PIN 8

Servo servo;
int currentAngle;

typedef struct {
  int angle;
  int distance;
} AngleAndDistance;

AngleAndDistance anglesAndDistances[180];

void setupServo() {
  servo.attach(SERVO_PIN);
  currentAngle = 0;
  servo.write(90);
}

void rotateRight(int angle) {
  servo.write(90 - angle);
  currentAngle = 90 - angle;
}

void rotateLeft(int angle) {
  servo.write(90 + angle);
  currentAngle = 90 + angle;
}

void scanArea(int d) {
  for(int i = 0; i < 90; i++) {
    rotateLeft(i);
    delay(d);
  }

  int currentIndex = 0;

  for(int i = 89; i >= 0; i--) {
    rotateLeft(i);
    anglesAndDistances[currentIndex].angle = currentAngle;
    anglesAndDistances[currentIndex++].distance = getDistance(mainSonar);
    delay(2 * d);
  }

  for(int i = 0; i < 90; i++) {
    rotateRight(i);
    anglesAndDistances[currentIndex].angle = currentAngle;
    anglesAndDistances[currentIndex++].distance = getDistance(mainSonar);
    delay(2 * d);
  }

  for(int i = 89; i >= 0; i--) {
    rotateRight(i);
    delay(d);
  }
}

int getCurrentAngle() {
  return currentAngle;
}

