#include <Servo.h>

#define DIR_PIN 7
#define MOTOR_PIN 8
#define DIR_FORWARD 0
#define DIR_BACKWARD 1

Servo dir;
Servo motor;

void setupMotorPins() {
 dir.attach(DIR_PIN);
 motor.attach(MOTOR_PIN);
 dir.write(90);
 motor.write(90);
 delay(1000);
}

int normalized(int value) {
  if(value > 180) {
    return 180;
  } else if(value < 0) {
    return 0;
  } else {
    return value;
  }
}

void moveForward(int value) {
  motor.write(normalized(90 + value));
  dir.write(90);
}

void moveBackward(int value) {
  motor.write(normalized(90 - value));
  dir.write(90);
}

void stopCar() {
  dir.write(90);
  motor.write(90);
}

void moveLeft(int dirValue, int motorValue, int direct) {
  dir.write(normalized(90 + dirValue));
  if(direct == DIR_FORWARD) {
    motor.write(normalized(90 + motorValue));
  } else if(direct == DIR_BACKWARD) {
    motor.write(normalized(90 - motorValue));
  }
}

void moveRight(int dirValue, int motorValue, int direct) {
  dir.write(normalized(90 - dirValue));
  if(direct == DIR_FORWARD) {
    motor.write(normalized(90 + motorValue));
  } else if(direct == DIR_BACKWARD) {
    motor.write(normalized(90 - motorValue));
  }
}

