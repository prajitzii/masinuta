#include <NewTone.h>

#define RED_LED_PIN A0
#define BLUE_LED_PIN A1
#define MAX_VALUE 1
#define MELODY_INITIAL_FREQUENCY 3000
#define STEP 100
#define MELODY_MAXIMUM_FREQUENCY 5000
#define BUZZER_PIN 9

int noteDurations = 16;
int value = 0;
int dir = 1;
int melody = MELODY_INITIAL_FREQUENCY;
int noteDuration = 500 / noteDurations;
int pauseBetweenNotes = noteDuration * .5;

void setupBuzzer()
{
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(BLUE_LED_PIN, OUTPUT);
}

void buzzerLoop()
{
  int redLightValue = map(value, 0, MAX_VALUE, 0, 255);
  int blueLightValue = map(MAX_VALUE - value, 0, MAX_VALUE, 0, 255);
  analogWrite(RED_LED_PIN, redLightValue);
  analogWrite(BLUE_LED_PIN, blueLightValue);
  value += dir;
  if(value > MAX_VALUE || value < 0)
  {
    dir = -dir;
  }
  if (melody < MELODY_MAXIMUM_FREQUENCY) {
    NewTone(BUZZER_PIN, melody, noteDuration);
    melody = melody + STEP;
    delay(pauseBetweenNotes);
    noNewTone(BUZZER_PIN);
  }
  else
  {
    melody = MELODY_INITIAL_FREQUENCY;
  }
}

