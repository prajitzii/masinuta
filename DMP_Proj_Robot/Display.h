#define LATCH_PIN 4
#define CLOCK_PIN 7
#define DATA_PIN 8

const unsigned char ssdlut[] = {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111, 0b11111111};
const unsigned char anodelut[] = {0b00000001, 0b00000010, 0b00000100, 0b00001000}; 

void setupDisplay() {
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
}

void displayNumber(int number) {
  int digits[4];
  for(int i = 0; i < 4; i++) {
    digits[3 - i] = number % 10;
    number /= 10;
  }
  for(int i = 0; i < 4; i++) {
    Serial.println(digits[i]);
  }
}

