#include "Motors.h"
#include "BTcomm.h"
#include "HC_SR04.h"
#include "FuzzyLogic.h"

int flag = 0;

void setup() {
  Serial.begin(9600);
  setupMotorPins();
  setupBT();
  setupRules();
}

void loop() {
  Serial.print(getMainSonarDistance());
  Serial.print(" ");
  moveForward(((int)floor(computeVelocity(getMainSonarDistance()) / 10)) % 17);
  Serial.println(((int)floor(computeVelocity(getMainSonarDistance()) / 10)) % 17);
  delay(10);  
}

