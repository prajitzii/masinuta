#include "Motors.h"
#include "BTcomm.h"
#include "HC_SR04.h"
#include "FuzzyLogic.h"

int flag = 0;
int period = 500;

void setup() {
  Serial.begin(9600);
  setupMotorPins();
  setupBT();
  setupRules();
  moveForward(0);
  delay(2000);
}

void loop() {
  float distance = getMainSonarDistance();
  int speed = (int)floor(computeVelocity(distance));
  moveForward(13);
  delay(speed);
  moveForward(11);
  delay(period - speed);
  Serial.println(distance);
}

